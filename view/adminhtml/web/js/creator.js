define([
    'jquery',
    'prototype',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
], function (jQuery, $, modal) {
    window.Creator = new Class.create();

    Creator.prototype = {
        options: {
            type: 'slide',
            buttons: []
        },
        apiCallCounter: 0,
        creatorElement: jQuery('#shirtplatform_creator'),
        currentEditedProductId: null,
        currentQuoteItemId: null,
        addToCartUrl: null,
        editItemUrl: null,
        maxApiCallCounter: 20,

        initialize: function () {
            this.creatorApiReady = false;
            this.modal = modal(this.options, this.creatorElement);
        },

        addToBasket: function (variantsData) {            
            var ajaxUrl = this.addToCartUrl;
            var postData = {data: {}}, postVariants = [], self = this;

            if (this.currentEditedProductId && this.editItemUrl) {
                ajaxUrl = this.editItemUrl;
                postData.data.editedProductId = this.currentEditedProductId;
            }
            else if (this.currentQuoteItemId) {
                postData.data.originalQuoteItemId = this.currentQuoteItemId;
            }

            jQuery.each(variantsData, function (key, variant) {
                postVariants.push({
                    id: variant['id'],
                    amount: variant['amount'],
                    price: variant['price'],
                    uuid: variant['uuid'],
                    color: {
                        color: {
                            name: variant['color']['color']['name']
                        },
                        id: variant['color']['id']
                    },
                    size: {
                        size: {
                            name: variant['size']['size']['name']
                        },
                        id: variant['size']['id']
                    },
                    design: {
                        product: {
                            id: variant['design']['product']['id']
                        }
                    },
                    order: {
                        id: variant['order']['id']
                    }
                });
            });
            postData.data.variants = postVariants;

            if (ajaxUrl) {
                jQuery.ajax({
                    url: ajaxUrl,
                    data: postData,
                    method: 'POST',
                    dataType: 'json',
                    success: function (result) {                        
                        if (result.error) {
                            alert(result.message);
                        }
                    },
                    complete: function () {
                        self.creatorElement.modal('closeModal');
                        self.currentEditedProductId = null;

                        if (window.order) {
                            window.order.itemsUpdate();
                        }
                    }
                });
            }
        },        

        editItem: function (orderedProductId, uuid) {
            if (this.creatorApiReady) {
                this.currentEditedProductId = orderedProductId;
                this.currentQuoteItemId = null;
                shirtplatform.creator.api.Creator.loadDesignedOrderedProduct(orderedProductId, uuid);
                this.creatorElement.modal('openModal');
            }                       
        },

        configureItem: function (quoteItemId, productId, colorId) {
            if (this.creatorApiReady) {
                this.currentEditedProductId = null;
                this.currentQuoteItemId = quoteItemId;
                shirtplatform.creator.api.Creator.loadProduct(productId, colorId);
                this.creatorElement.modal('openModal');
            }                        
        },                

        creatorPreInitializeCallback: function (xmlSnippet, scriptUrl) {
            this.creatorElement.html(xmlSnippet.content);            
        }
    }

    creator = new Creator();
});