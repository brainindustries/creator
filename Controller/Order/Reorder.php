<?php

namespace Shirtplatform\Creator\Controller\Order;

use Magento\Framework\Registry;
use Shirtplatform\Core\Helper\Order;
use Magento\Sales\Controller\AbstractController\OrderLoaderInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Model\CartFactory;
use Shirtplatform\Creator\Helper\Data;
use \shirtplatform\entity\order\Order as PlatformOrder;

class Reorder implements \Magento\Framework\App\ActionInterface
{
    /**
     * @var CartFactory
     */
    protected $_cartFactory;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var Order
     */
    protected $_orderHelper;

    /**
     * @var OrderLoaderInterface
     */
    protected $_orderLoader;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var RedirectFactory
     */
    protected $_redirectFactory;

    /**
     * @param CartFactory $cartFactory
     * @param CheckoutSession $checkoutSession
     * @param Data $helper
     * @param Registry $registry
     * @param ManagerInterface $messageManager
     * @param Order $orderHelper
     * @param OrderLoaderInterface $orderLoader
     * @param RequestInterface $request
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        CartFactory $cartFactory,
        CheckoutSession $checkoutSession,
        Data $helper,
        Registry $registry,
        ManagerInterface $messageManager,
        Order $orderHelper,
        OrderLoaderInterface $orderLoader,
        RequestInterface $request,
        RedirectFactory $redirectFactory
    ) {
        $this->_cartFactory = $cartFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_helper = $helper;
        $this->_coreRegistry = $registry;
        $this->_messageManager = $messageManager;
        $this->_orderHelper = $orderHelper;
        $this->_orderLoader = $orderLoader;
        $this->_request = $request;
        $this->_redirectFactory = $redirectFactory;
    }

    public function execute()
    {
        $result = $this->_orderLoader->load($this->_request);
        if ($result instanceof \Magento\Framework\Controller\ResultInterface) {
            return $result;
        }
        $order = $this->_coreRegistry->registry('current_order');
        $resultRedirect = $this->_redirectFactory->create();

        //create platform order duplicate
        $itemMapper = $this->_orderHelper->createPlatformOrderDuplicate($order);
        $platformOrder = $this->_coreRegistry->registry('session_platform_order');
        if ($platformOrder == null) {
            $this->_messageManager->addErrorMessage(__('Platform order cannot be created at the moment'));
            return $resultRedirect->setPath('checkout/cart');
        }

        $origPlatformOrder = PlatformOrder::find($order->getShirtplatformId());
        $productsToReorder = $this->_helper->getProductsToReorder($order->getAllItems(), $origPlatformOrder);
        if (empty($productsToReorder)) {
            $this->_messageManager->addErrorMessage(__('None of the products in this order can be reordered.'));
            return $resultRedirect->setPath('sales/order/history');
        }

        $cart = $this->_cartFactory->create();
        foreach ($productsToReorder as $item) {
            try {
                $this->_coreRegistry->register('current_order_item', $item);
                $cart->addOrderItem($item);
                $this->_coreRegistry->unregister('current_order_item');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                if ($this->_checkoutSession->getUseNotice(true)) {
                    $this->_messageManager->addNoticeMessage($e->getMessage());
                } else {
                    $this->_messageManager->addErrorMessage($e->getMessage());
                }
                return $resultRedirect->setPath('sales/order/history');
            } catch (\Exception $e) {
                $this->_messageManager->addExceptionMessage($e, __('We can\'t add this item to your shopping cart right now.'));
                return $resultRedirect->setPath('checkout/cart');
            }
        }

        $cart->save();
        return $resultRedirect->setPath('checkout/cart');
    }
}