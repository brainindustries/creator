<?php

namespace Shirtplatform\Creator\Plugin\Quote\Item;

use Shirtplatform\Core\Model\Config\Source\ProductType;

class Compare {
    /**
     * Compare two quote items. Return false if one of the items is designed base product,
     * because the two items may have the same configuration, but different designs.
     *
     * @access public
     * @param \Magento\Quote\Model\Quote\Item $target
     * @param \Magento\Quote\Model\Quote\Item $compared
     * @return bool
     */
    public function afterCompare($subject, $result, $target, $compared) {        
        if ($result) {
            if ($target->getShirtplatformId() and $target->getProduct()->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                return false;
            }
            if ($compared->getShirtplatformId() and $compared->getProduct()->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                return false;
            }
        }
                        
        return $result;
    }
}