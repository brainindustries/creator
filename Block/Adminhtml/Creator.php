<?php

namespace Shirtplatform\Creator\Block\Adminhtml;

class Creator extends \Magento\Backend\Block\Template {

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    protected $_helper;

    /**
     * Session quote
     *
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $_sessionQuote;    

    /**
     * 
     * @param \Shirtplatform\Core\Helper\Data $helper
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote     
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(\Shirtplatform\Core\Helper\Data $helper,
                                \Magento\Backend\Model\Session\Quote $sessionQuote,                                
                                \Magento\Backend\Block\Template\Context $context,
                                array $data = []) {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->_sessionQuote = $sessionQuote;        
    }

    /**
     * Get URL of creator javascript file
     * 
     * @access public
     * @return string
     */
    public function getCreatorJavascriptFile() {
        $store = $this->_sessionQuote->getStore();
        $url = $this->_scopeConfig->getValue('shirtplatform/general/url');
        $url = preg_replace('/rest\/$/', '', $url);
        $url .= 'spring/magento/creator-app?shop=' . $this->_helper->getConnectionUniqUrl($store);
        return $url;
    }
    
}
