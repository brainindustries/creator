<?php

namespace Shirtplatform\Creator\Plugin\Catalog\Product;

use Magento\Framework\View\Result\Page as ResultPage;

class AddLayoutHandle {

    /**
     * Init layout for viewing product page. Add shirtplatform_creator_product
     * handle for base products
     *
     * @access public
     * @param \Magento\Catalog\Helper\Product\View $subject
     * @param \Magento\Framework\View\Result\Page $resultPage
     * @param \Magento\Catalog\Model\Product $product
     * @param null|\Magento\Framework\DataObject $params
     * @return \Magento\Catalog\Helper\Product\View
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function beforeInitProductLayout($subject,
                                            ResultPage $resultPage,
                                            $product,
                                            $params = null) {
        if ($product->getShirtplatformProductType() == \Shirtplatform\Core\Model\Config\Source\ProductType::BASE_PRODUCT) {
            $resultPage->addHandle('shirtplatform_creator_product');
        }
        return [$resultPage, $product, $params];
    }

}
