<?php

namespace Shirtplatform\Creator\Helper;

use shirtplatform\entity\order\DesignedOrderedProduct;
use Shirtplatform\Core\Model\Config\Source\ProductType;

class Cart extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $_configurableProduct;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Shirtplatform\Creator\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $_quoteRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * 
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Shirtplatform\Creator\Helper\Data $helper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(\Magento\Framework\App\State $appState,
                                \Magento\Checkout\Model\Cart $cart,
                                \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct,
                                \Magento\Framework\Registry $coreRegistry,
                                \Magento\Eav\Model\Config $eavConfig,
                                \Shirtplatform\Creator\Helper\Data $helper,
                                \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
                                \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
                                \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
                                \Magento\Framework\App\Helper\Context $context) {
        parent::__construct($context);
        $this->_appState = $appState;
        $this->_cart = $cart;
        $this->_configurableProduct = $configurableProduct;
        $this->_coreRegistry = $coreRegistry;
        $this->_eavConfig = $eavConfig;
        $this->_helper = $helper;
        $this->_productRepository = $productRepository;
        $this->_quoteRepository = $quoteRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Add shirtplatform variants to magento cart
     * 
     * @access public
     * @param \Magento\Checkout\Model\Cart|\Magento\Quote\Model\Quote $itemHolder
     * @param array $variants
     * @param bool $saveCart
     * @return int number of variants added to cart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addVariants($itemHolder,
                                $variants,
                                $saveCart = true) {        
        $sizeAttribute = $this->_eavConfig->getAttribute('catalog_product', 'size');
        $colorAttribute = $this->_eavConfig->getAttribute('catalog_product', 'color');
        $count = 0;

        foreach ($variants as $_variant) {
            $colorId = $_variant['color']['id'];
            $sizeId = $_variant['size']['id'];
            
            //it's important to clean repository cache and reload the product again, otherwise if we have
            //more configurable variants, they won't be correctly added to quote_items table, only one
            //parent product will be added
            //it also causes problems that the product options are not added for all simple products in quote_item_option
            $this->_productRepository->cleanCache();
            $this->_searchCriteriaBuilder->addFilter('shirtplatform_id', $_variant['design']['product']['id']);
            $this->_searchCriteriaBuilder->setPageSize(1);
            $searchCriteria = $this->_searchCriteriaBuilder->create();
            $productList = $this->_productRepository->getList($searchCriteria)->getItems();

            if (empty($productList)) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__("The product that was requested doesn't exist"));
            }

            $product = reset($productList);
//                $product = $this->_productRepository->getById($data['magento_product_id'], false, $store->getId());

            $childProducts = $this->_configurableProduct->getUsedProducts($product);
            $selectedChild = null;
            
            foreach ($childProducts as $child) {
                if ($child->getShirtplatformColorId() == $colorId and $child->getShirtplatformSizeId() == $sizeId) {
                    $selectedChild = $child;
                    break;
                }
            }

            if ($selectedChild) {                
                $params = [
                    'product' => $product->getId(),
                    'qty' => $_variant['amount'],
                    'super_attribute' => [
                        $colorAttribute->getId() => $selectedChild->getColor(),
                        $sizeAttribute->getId() => $selectedChild->getSize()
                    ],
                ];

                $this->_coreRegistry->register('current_cart_variant_data', $_variant);
                $request = new \Magento\Framework\DataObject($params);
                $itemHolder->addProduct($product, $request);
                $this->_coreRegistry->register('updateManualServices', true, true);
                $this->_coreRegistry->unregister('current_cart_variant_data');
                $count += $_variant['amount'];

                if (isset($_variant['order']['basketId'])) {
                    if ($itemHolder instanceof \Magento\Quote\Model\Quote) {
                        $itemHolder->setShirtplatformBasketId($_variant['order']['basketId']);
                    }
                    else {
                        $itemHolder->getQuote()->setShirtplatformBasketId($_variant['order']['basketId']);
                    }
                }
            }
        }

        if ($saveCart) {
            if ($itemHolder instanceof \Magento\Quote\Model\Quote) {
                $itemHolder->collectTotals();
                $this->_quoteRepository->save($itemHolder);
            }
            else {
                $itemHolder->save();
            }
        }

        return $count;
    }

    /**
     * Edit shirtplatform item - remove old item from magento cart and shirtplatform order
     * and add new variants
     * 
     * @access public
     * @param \Magento\Checkout\Model\Cart|\Magento\Quote\Model\Quote $itemHolder
     * @param int $editedProductId
     * @param array $variants
     * @return int number of variants added to cart
     */
    public function editItem($itemHolder,
                             $editedProductId,
                             $variants) {
        //we have to add variants first, because if a quote has 0 items, it automatically
        //removes all addresses => no shipping methods available
        $result = $this->addVariants($itemHolder, $variants, false);
        $itemOriginalOrderId = null;

        $quote = $itemHolder;
        if ($itemHolder instanceof \Magento\Checkout\Model\Cart) {
            $quote = $itemHolder->getQuote();
        }

        //remove from magento cart
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($item->getShirtplatformId() == $editedProductId) {
                $itemOriginalOrderId = $item->getShirtplatformOrigOrderId();
                $quote->removeItem($item->getId());
            }
        }

        //remove from shirtplatform - moved to core observer
//        if ($itemOriginalOrderId and $this->_appState->getAreaCode() != 'adminhtml') {
//            $this->_helper->shirtplatformAuth($quote->getStoreId());
//            $shirtplatformProduct = DesignedOrderedProduct::find($editedProductId, $itemOriginalOrderId);
//
//            if ($shirtplatformProduct) {
//                $shirtplatformProduct->__delete();
//            }
//        }

        if ($itemHolder instanceof \Magento\Quote\Model\Quote) {
            $itemHolder->collectTotals();
            $this->_quoteRepository->save($itemHolder);
        }
        else {
            $itemHolder->save();
        }

        return $result;
    }

    /**
     * Set variant data to quote item when adding to cart
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param array $variant
     */
    public function setVariantDataToQuoteItem($item,
                                              $variant) {
        $item->setShirtplatformId($variant['id']);
        $item->setShirtplatformProductType(ProductType::BASE_PRODUCT);
        $item->setShirtplatformOrigOrderId($variant['order']['id']);
        $item->setShirtplatformUuid($variant['uuid']);
        $item->setCustomPrice($variant['price']);
        $item->setOriginalCustomPrice($variant['price']);
        $item->getProduct()->setIsSuperMode(true);
    }

}
