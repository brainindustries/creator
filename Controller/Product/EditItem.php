<?php

namespace Shirtplatform\Creator\Controller\Product;

use Magento\Framework\Controller\ResultFactory;
use Shirtplatform\Creator\Controller\AbstractController\Item;

class EditItem extends Item {    

    /**
     * Edit shirtplatform product action
     * 
     * @access public
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute() {
        if (!$this->_request->isAjax()) {
            $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('/');            
            return $resultRedirect;
        }

        $response = [];
        $resultJson = $this->_resultFactory->create(ResultFactory::TYPE_JSON);
        $data = $this->_getData();        

        if (empty($data['editedProductId']) or empty($data['variants'])) {
            if ($data instanceof \Zend\Stdlib\ParametersInterface) {
                $params = $data->toString();
            }
            else {
                $params = $data;
            }

            $response['error'] = 1;
            $response['message'] = __('Cannot edit this product');
            $logMessage = 'Cannot edit product. Post params:';
            $this->_dataHelper->logMessage($logMessage);
            $this->_dataHelper->logMessage($data);
            $this->_dataHelper->logError($logMessage);
            $this->_dataHelper->logError($data);
            $resultJson->setData($response);
            return $resultJson;
        }

        try {
            $count = $this->_cartHelper->editItem($this->_cart, $data['editedProductId'], $data['variants']);
            
            $editedItems = [];
            foreach ($this->_cart->getQuote()->getAllVisibleItems() as $item) {
                if ($this->_wasItemEdited($item, $data['variants'])) {
                    $editedItems[] = $item;
                }
            }

            $this->_eventManager->dispatch(
                'shirtplatform_creator_item_edit_complete',
                ['items' => $editedItems]
            );

            if ($count) {
                $this->_messageManager->addSuccessMessage(__('Product has been edited'));
            }
            else {
                $this->_messageManager->addSuccessMessage(__('Product could not be edited'));
            }
        } catch (\Exception $ex) {
            $response['error'] = 1;
            $response['message'] = $ex->getMessage();
            $resultJson->setData($response);
            $this->_messageManager->addError($ex->getMessage());
            return $resultJson;
        }

        $response['success'] = 1;
        $response['error'] = 0;
        $resultJson->setData($response);
        return $resultJson;
    }

    /**
     * Was this item edited
     *
     * @access private
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param  array $addedItems variant data sent to this controller
     * @return boolean
     */
    private function _wasItemEdited($item, $addedItems)
    {
        foreach ($addedItems as $variantData) {
            if ($variantData['id'] == $item->getShirtplatformId()) {
                return true;
            }
        }

        return false;
    }
}
