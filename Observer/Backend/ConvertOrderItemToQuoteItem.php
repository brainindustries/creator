<?php

namespace Shirtplatform\Creator\Observer\Backend;

use Shirtplatform\Core\Model\Config\Source\ProductType;

class ConvertOrderItemToQuoteItem implements \Magento\Framework\Event\ObserverInterface {

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * Quote session object
     *
     * @var \Magento\Backend\Model\Session\Quote
     */
    private $_session;

    /**
     * 
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(\Magento\Framework\App\RequestInterface $request,
                                \Magento\Backend\Model\Session\Quote $session) {
        $this->_request = $request;
        $this->_session = $session;
    }

    /**
     * Set shirtplatform_uuid for quote item
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $orderItem = $observer->getOrderItem();
        $quoteItem = $observer->getQuoteItem();
        $actionName = $this->_request->getFullActionName();

//        if ($actionName == 'sales_order_edit_start') {
//            $quoteItem->setShirtplatformUuid($orderItem->getShirtplatformUuid());
//        }
//        elseif (in_array($actionName, ['sales_order_create_reorder', 'shirtplatform_rma_order_exchange_start'])) {
        if (in_array($actionName, ['sales_order_create_reorder', 'shirtplatform_rma_order_exchange_start'])) {
            if ($orderItem->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                $order = $this->_session->getOrder();
                $quoteItem->setShirtplatformId($orderItem->getShirtplatformId());
                $quoteItem->setShirtplatformProductType($orderItem->getShirtplatformProductType());
                $quoteItem->setShirtplatformOrigOrderId($order->getShirtplatformId());
            }
        }
    }

}
