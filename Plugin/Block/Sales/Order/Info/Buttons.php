<?php

namespace Shirtplatform\Creator\Plugin\Block\Sales\Order\Info;

use Shirtplatform\Core\Model\Config\Source\ProductType;

class Buttons {
    /**
     * Get reorder URL
     * 
     * @param \Magento\Sales\Block\Order\Recent $subject
     * @param \Closure $proceed
     * @param \Magento\Sales\Model\Order $order
     * @return string
     */
    public function aroundGetReorderUrl($subject, $proceed, $order) {        
        if ($order->getShirtplatformId()) {
            foreach ($order->getAllItems() as $item) {
                if ($item->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                    return $subject->getUrl('shirtplatform_creator/order/reorder', ['order_id' => $order->getId()]);
                }
            }            
        }
        
        return $proceed($order);
    }
}