<?php

namespace Shirtplatform\Creator\Plugin\Sales\Model\AdminOrder;

use Shirtplatform\Core\Helper\Data as CoreHelper;
use Shirtplatform\Creator\Helper\Data;
use Magento\Sales\Model\Order\Item;
use Magento\Framework\App\RequestInterface;
use shirtplatform\entity\order\Order;

class Create
{
    /**
     * @var CoreHelper
     */
    private $_coreHelper;

    /**
     * @var Data
     */
    private $_helper;

    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * @var Item[]
     */
    private $_productsToReorder = [];

    /**
     * @param CoreHelper
     * @param Data
     * @param RequestInterface
     */
    public function __construct(
        CoreHelper $_coreHelper,
        Data $helper,
        RequestInterface $request
    ) {
        $this->_coreHelper = $_coreHelper;
        $this->_helper = $helper;
        $this->_request = $request;
    }

    public function beforeInitFromOrder($subject, $order)
    {
        if (!$order->getShirtplatformId()) {
            return [$order];
        }

        $this->_coreHelper->shirtplatformAuth($order->getStoreId());
        $platformOrder = Order::find($order->getShirtplatformId());
        $this->_productsToReorder = $this->_helper->getProductsToReorder($order->getAllItems(), $platformOrder);
        return [$order];
    }

    public function aroundInitFromOrderItem($subject, $proceed, $orderItem, $qty)
    {
        $isReorder = $this->_request->getFullActionName() === 'sales_order_create_reorder';
        $isConfigurable = $orderItem->getProductType() === \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE;

        if (!$isReorder || $orderItem === null || !$isConfigurable) {
            return $proceed($orderItem, $qty);
        }

        $canReorder = false;
        foreach ($this->_productsToReorder as $_productToReorder) {
            if ($_productToReorder === null) {
                continue;
            }

            if ($orderItem->getId() == $_productToReorder->getId()) {
                $canReorder = true;
                break;
            }
        }

        if ($canReorder) {
            return $proceed($orderItem, $qty);
        }
    }
}