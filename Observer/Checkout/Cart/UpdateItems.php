<?php

namespace Shirtplatform\Creator\Observer\Checkout\Cart;

use Magento\Framework\Event\ObserverInterface;

class UpdateItems implements ObserverInterface {
    
    /**     
     * @var \Shirtplatform\Core\Helper\Order
     */
    private $_orderHelper;
    
    /**
     * 
     * @param \Shirtplatform\Core\Helper\Order $orderHelper
     */
    public function __construct(\Shirtplatform\Core\Helper\Order $orderHelper) {
        $this->_orderHelper = $orderHelper;
    }
    
    /**
     * Update designed products in platform when quote items in magento are updated
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $cart = $observer->getCart();
        $data = $observer->getInfo()->getData();
        $quote = $cart->getQuote();
        $this->_orderHelper->updateDesignedProductQty($quote, $data);
    }
}