<?php

namespace Shirtplatform\Creator\Controller\Product;

use Magento\Framework\Controller\ResultFactory;
use shirtplatform\resource\PublicResource;
use Shirtplatform\Creator\Controller\AbstractController\Item;

class AddToCart extends Item
{    
    /**
     * Add shirtplatform variants to magento cart
     * 
     * @access public
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        if (!$this->_request->isAjax()) {
            $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('/');            
            return $resultRedirect;
        }

        $response = [];
        $resultJson = $this->_resultFactory->create(ResultFactory::TYPE_JSON);
        $data = $this->_getData();        

        if (empty($data['variants'])) {
            if ($data instanceof \Zend\Stdlib\ParametersInterface) {
                $params = $data->toString();
            } else {
                $params = $data;
            }

            $response['error'] = 1;
            $response['message'] = __('Cannot add product to cart');
            $logMessage = 'Cannot add product to cart. Post params:';
            $this->_dataHelper->logMessage($logMessage);
            $this->_dataHelper->logMessage($data);
            $this->_dataHelper->logError($logMessage);
            $this->_dataHelper->logError($data);
            $resultJson->setData($response);
            return $resultJson;
        }

        $message = '';

        try {
            $count = $this->_cartHelper->addVariants($this->_cart, $data['variants']);
            if ($count == 0) {
                $this->_messageManager->addErrorMessage(__('The product could not be added to your shopping cart'));
            } else {
                if ($count == 1) {
                    $message = __('You added 1 product to your shopping cart.');
                } elseif ($count >= 2 and $count <= 4) {
                    $message = __('You added ' . $count . ' products to your shopping cart.');
                } else {
                    $message = __('You added %1 products to your shopping cart.', $count);
                }

                //add first view image
                if (isset($data['variants'][0]['viewId'])) {
                    $response['image'] = PublicResource::getOrderedProductImageUrl(
                        $data['variants'][0]['id'],
                        $data['variants'][0]['uuid'],
                        $data['variants'][0]['viewId'],
                        70,
                        70
                    );
                }

                $addedItems = [];

                //add information about added variants                
                foreach ($this->_cart->getQuote()->getAllVisibleItems() as $item) {
                    if ($this->_wasItemJustAddedToCart($item, $data['variants'])) {
                        $response['added_products'][$item->getShirtplatformId()] = [
                            'id' => $item->getProductId(),
                            'price_incl_tax' => $item->getPriceInclTax(),
                            'qty' => $item->getQty()
                        ];
                        $addedItems[] = $item;
                    }
                }

                $this->_eventManager->dispatch(
                    'shirtplatform_creator_add_to_cart_complete',
                    ['items' => $addedItems]
                );

                $this->_messageManager->addSuccessMessage($message);
            }
        } catch (\Exception $ex) {
            $response['error'] = 1;
            $response['message'] = $ex->getMessage();
            $resultJson->setData($response);
            $this->_messageManager->addErrorMessage($ex->getMessage());
            return $resultJson;
        }

        $response['success'] = 1;
        $response['error'] = 0;
        $response['message'] = $message;
        $resultJson->setData($response);
        return $resultJson;
    }
    
    /**
     * Was this item just added to cart
     *
     * @access private
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param  array $addedItems variant data sent to this controller
     * @return boolean
     */
    private function _wasItemJustAddedToCart($item, $addedItems)
    {
        foreach ($addedItems as $variantData) {
            if ($variantData['id'] == $item->getShirtplatformId()) {
                return true;
            }
        }

        return false;
    }
}
