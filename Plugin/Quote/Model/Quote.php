<?php

namespace Shirtplatform\Creator\Plugin\Quote\Model;

class Quote
{
    /**
     * @var \Shirtplatform\Creator\Helper\Data
     */
    private $helper;

    /**
     * Data that an item has to have in order to have the right connection with Shirtplatform
     * 
     * @var array
     */
    private $shirtplatformData = [];

    /**
     * @param \Shirtplatform\Creator\Helper\Data $helper
     */
    public function __construct(\Shirtplatform\Creator\Helper\Data $helper) {
        $this->helper = $helper;
    }

    /**
     * Save shirtplatform data before updating an item, because the system doesn't know about them
     * when adding an item to the cart in the original method
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote $subject
     * @param int $itemId
     * @param \Magento\Framework\DataObject $buyRequest
     * @param null|array|\Magento\Framework\DataObject $params
     * @return null
     */
    public function beforeUpdateItem($subject, $itemId, $buyRequest, $params = null)
    {
        $item = $subject->getItemById($itemId);
        if ($item) {
            $this->shirtplatformData = [];
            $this->shirtplatformData['shirtplatform_id'] = $item->getShirtplatformId();
            $this->shirtplatformData['shirtplatform_product_type'] = $item->getShirtplatformProductType();
            $this->shirtplatformData['shirtplatform_orig_order_id'] = $item->getShirtplatformOrigOrderId();
            $this->shirtplatformData['shirtplatform_uuid'] = $item->getShirtplatformUuid();
            $this->shirtplatformData['custom_price'] = $item->getCustomPrice();
            $this->shirtplatformData['original_custom_price'] = $item->getOriginalCustomPrice();
            $this->helper->logMessage('saving shirtplatform data for item ' . $itemId . ': ' . json_encode($this->shirtplatformData));

            $msg = "\n";
            foreach (debug_backtrace() as $l) {
                if (isset($l['file'])) {
                    $msg .= $l['file'];
                }
                if (isset($l['line'])) {
                    $msg .= ' ('. $l['line'] .')';
                }
                if (isset($l['class'])) {
                    $msg .= '   '. $l['class'];
                }
                if (isset($l['function'])) {
                    $msg .= '   '. $l['function'] .'()';
                }
                $msg .= "\n";
            }
            $this->helper->logMessage($msg);
        }

        return null;
    }

    /**
     * Set shirtplatform data to the item after updating it.
     * 
     * For now it just logs the data as I'm debugging if this causes the issue.
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote $subject
     * @param \Magento\Quote\Model\Quote\Item $result
     * @param int $itemId
     * @param \Magento\Framework\DataObject $buyRequest
     * @param null|array|\Magento\Framework\DataObject $params
     * @return \Magento\Quote\Model\Quote\Item
     */
    public function afterUpdateItem($subject, $result, $itemId, $buyRequest, $params = null)
    {
        $msg = '';
        foreach ($this->shirtplatformData as $key => $value) {
            if (empty($result->getData($key))) {
                $result->setData($key, $value);
                $msg .= $key . ' was empty. Setting to ' . $value . "\n";
            }
        }

        $this->helper->logMessage('afterUpdateItem for item ' . $itemId . '. This data would be set ' . $msg);

        $msg = '';
        foreach ($result->getData() as $key => $value) {
            if (is_scalar($value)) {
                $msg .= $key . ' => ' . $value . "\n";
            }
        }
        $this->helper->logMessage($msg);
        return $result;
    }
}
