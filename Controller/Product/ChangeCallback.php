<?php

namespace Shirtplatform\Creator\Controller\Product;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\App\Action\Context;

/**
 * Class ChangeCallback
 * @package Shirtplatform\Creator\Controller\Product
 */
class ChangeCallback extends \Magento\Framework\App\Action\Action
{

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var \Magento\Framework\View\Result\PageFactory */
    private $pageFactory;

    /** @var \Magento\Framework\Registry */
    private $registry;

    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    private $resultJsonFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /** @var CategoryRepository */
    private $categoryRepository;

    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CategoryRepository $categoryRepository
    ) {
        parent::__construct($context);
        $this->productRepository = $productRepository;
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $productId = $this->getRequest()->getParam('productId');

        try
        {
            $product = $this->productRepository->get('BP' . $productId, false, $this->storeManager->getStore()->getId());
            //we have to get product URL here, before category path is added to it
            $productUrl = $product->getProductUrl();            
        }
        catch(\Exception $e)
        {
            return $this->resultJsonFactory->create()->setData([
                'state' => 0
            ]);
        }

        $this->getRequest()->setParams(['id' => $product->getId()]);

        $this->registry->register('product', $product);
        $this->registry->register('current_product', $product);

        $category = $this->categoryRepository->get($this->storeManager->getStore()->getRootCategoryId());
        $this->registry->register('current_category', $category);

        $this->getRequest()->setRouteName('catalog');
        $this->getRequest()->setControllerName('product');
        $this->getRequest()->setActionName('view');
        $breadcrumbsBlock = $this->pageFactory->create()->getLayout()->getBlock('breadcrumbs');        

        return $this->resultJsonFactory->create()->setData([
            'state' => 1,
            'url' => $productUrl,
            'title' => ($product->getMetaTitle() != null) ? $product->getMetaTitle() : $product->getName(),
            'breadcrumbs' => $breadcrumbsBlock ? $breadcrumbsBlock->toHtml() : ''
        ]);
    }

}
