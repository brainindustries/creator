<?php

namespace Shirtplatform\Creator\Plugin\Checkout\Block\Cart;

use Shirtplatform\Core\Model\Config\Source\ProductType;

class AbstractCart {        
    
    /**
     * Get item row html. Use "designed" item renderer for shirtplatform base products
     * 
     * @access public
     * @param \Magento\Checkout\Block\Cart\AbstractClass $subject
     * @param Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return string
     */
    public function aroundGetItemHtml($subject, $proceed, $item) {        
        if ($item->getShirtplatformId()) {
//            $product = $item->getProduct();            
            
            if ($item->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                $renderer = $subject->getItemRenderer('designed_product')->setItem($item);
                return $renderer->toHtml();
            }
        }
        
        return $proceed($item);
    }
}