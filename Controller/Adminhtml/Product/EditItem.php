<?php

namespace Shirtplatform\Creator\Controller\Adminhtml\Product;

class EditItem extends \Magento\Backend\App\Action {

    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magento_Sales::actions_edit';

    /**
     * @var \Shirtplatform\Creator\Helper\Cart
     */
    protected $_cartHelper;

    /**
     * @var \Shirtplatform\Creator\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * Quote session object
     *
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $_sessionQuote;

    /**
     * 
     * @param \Shirtplatform\Creator\Helper\Cart $cartHelper
     * @param \Shirtplatform\Creator\Helper\Data $dataHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(\Shirtplatform\Creator\Helper\Cart $cartHelper,
                                \Shirtplatform\Creator\Helper\Data $dataHelper,
                                \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
                                \Magento\Backend\Model\Session\Quote $sessionQuote,
                                \Magento\Backend\App\Action\Context $context) {
        parent::__construct($context);
        $this->_cartHelper = $cartHelper;
        $this->_dataHelper = $dataHelper;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_sessionQuote = $sessionQuote;
    }

    /**
     * Edit shirtplatform product action
     * 
     * @access public
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute() {        
        if (!$this->_request->isAjax()) {
            $this->_redirect('/');
            return;
        }

        $response = [];
        $resultJson = $this->_resultJsonFactory->create();
        $data = $this->_request->getPost('data');        
        
        if (empty($data['editedProductId']) or empty($data['variants'])) {
            if ($data instanceof \Zend\Stdlib\ParametersInterface) {
                $params = $data->toString();
            }
            else {
                $params = $data;
            }

            $response['error'] = 1;
            $response['message'] = __('Cannot edit this product');
            $logMessage = 'Cannot edit product. Post params:';
            $this->_dataHelper->logMessage($logMessage);
            $this->_dataHelper->logMessage($data);
            $this->_dataHelper->logError($logMessage);
            $this->_dataHelper->logError($data);
            $resultJson->setData($response);
            return $resultJson;
        }

        try {
            $count = $this->_cartHelper->editItem($this->_sessionQuote->getQuote(), $data['editedProductId'], $data['variants']);            
        } catch (\Exception $ex) {
            $response['error'] = 1;
            $response['message'] = $ex->getMessage();
            $resultJson->setData($response);
            $this->messageManager->addError($ex->getMessage());
            return $resultJson;
        }

        $response['success'] = 1;
        $response['error'] = 0;
        $response['message'] = 'The product has been edited';
        $resultJson->setData($response);
        return $resultJson;
    }

}
