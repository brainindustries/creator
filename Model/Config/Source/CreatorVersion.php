<?php

namespace Shirtplatform\Creator\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CreatorVersion implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'old', 'label' => 'Old'],
            ['value' => 'new', 'label' => 'New']
        ];
    }
}