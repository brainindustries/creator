<?php

namespace Shirtplatform\Creator\Model\Cart;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Shirtplatform\Core\Model\Config\Source\ProductType;

class CheckoutSummaryConfigProvider implements ConfigProviderInterface {

    /**
     * @var CheckoutSession
     */
    private $_checkoutSession;

    /**
     * @var \Shirtplatform\Core\Helper\Image
     */
    private $_imageHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param CheckoutSession $checkoutSession
     * @param \Shirtplatform\Core\Helper\Image $imageHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(CheckoutSession $checkoutSession,
                                \Shirtplatform\Core\Helper\Image $imageHelper,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->_checkoutSession = $checkoutSession;
        $this->_imageHelper = $imageHelper;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Add design and mouseover images to checkout config for base products
     * 
     * @access public
     * @return array
     */
    public function getConfig() {
        $output = [
            'design_images' => [],
            'mouseover_images' => []
        ];

        $quote = $this->_checkoutSession->getQuote();
        $excludeHumanView = $this->_scopeConfig->getValue(
                'shirtplatform/images/exclude_human_view_from_cart_summary', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $quote->getStoreId()
        );
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($item->getShirtplatformId()) {

                if ($item->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                    $images = $this->_imageHelper->getDesignImages($item, ['width' => 70, 'height' => 70], $excludeHumanView);
                    $mouseoverImages = $this->_imageHelper->getDesignImages($item, ['width' => 150, 'height' => 150], $excludeHumanView);
                    $output['design_images']['item_' . $item->getId()] = $images;
                    $output['mouseover_images']['item_' . $item->getId()] = $mouseoverImages;
                }
            }
        }

        return $output;
        ;
    }

}
