<?php

namespace Shirtplatform\Creator\Observer\Checkout\Cart;

use Magento\Framework\Event\ObserverInterface;
use Shirtplatform\Core\Model\Config\Source\ProductType;

class ProductAddAfter implements ObserverInterface {

    /**
     * Next shirtplatform_design_group value for non team creator product. Products
     * that are added at the same time usually have the same design and differ only 
     * in sizes => they can be grouped together in cart.
     * 
     * @var int
     */
    private $_nextNonTeamProductDesignGroup;
    
    /**
     * @var \Shirtplatform\Creator\Helper\Cart
     */
    protected $_cartHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * 
     * @param \Shirtplatform\Creator\Helper\Cart $cartHelper     
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(\Shirtplatform\Creator\Helper\Cart $cartHelper,
                                \Magento\Framework\Registry $registry) {
        $this->_cartHelper = $cartHelper;
        $this->_coreRegistry = $registry;
    }

    /**
     * Add shirtplatform_id, shirtplatform_product_type, shirtplatform_orig_order_id, 
     * shirtplatform_uuid and custom price to quote item
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $quoteItem = $observer->getQuoteItem();        
        
        $variant = $this->_coreRegistry->registry('current_cart_variant_data');
        if ($variant) {
            $this->_cartHelper->setVariantDataToQuoteItem($quoteItem, $variant);
            
            //set shirtplatform_design_group
            if ($quoteItem->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                $quote = $quoteItem->getQuote();
                $maxDesignGroup = 0;
                
                foreach ($quote->getAllVisibleItems() as $item) {                                        
                    if ($item->getShirtplatformDesignGroup() > $maxDesignGroup) {
                        $maxDesignGroup = $item->getShirtplatformDesignGroup();
                    }
                }
                                
                $nextDesignGroup = $maxDesignGroup + 1;                
                if (isset($variant['teamDesignCollection'])) {
                    $quoteItem->setShirtplatformDesignGroup($nextDesignGroup);
                }
                else {
                    if (empty($this->_nextNonTeamProductDesignGroup)) {
                        $this->_nextNonTeamProductDesignGroup = $nextDesignGroup;                        
                    }                    
                    $quoteItem->setShirtplatformDesignGroup($this->_nextNonTeamProductDesignGroup);
                }
            }
        }
        //for reorder action
        else {
            $orderItem = $this->_coreRegistry->registry('current_order_item');
            $itemMapper = $this->_coreRegistry->registry('platform_order_item_mapper');
            $platformOrder = $this->_coreRegistry->registry('session_platform_order');
            
            if ($orderItem and $itemMapper and $platformOrder) {
                $itemShirtplatformId = $orderItem->getShirtplatformId();

                if ($itemShirtplatformId and isset($itemMapper[$itemShirtplatformId])) {
                    $quoteItem->setShirtplatformId($itemMapper[$itemShirtplatformId]->id);
                    $quoteItem->setShirtplatformOrigOrderId($platformOrder->id);
                    $quoteItem->setShirtplatformUuid($itemMapper[$itemShirtplatformId]->uuid);
                    $this->_coreRegistry->register('updateManualServices', true, true);
                    
                    if (abs($orderItem->getPrice() - $orderItem->getOriginalPrice()) > 0.001) {
                        $quoteItem->setCustomPrice($orderItem->getPrice());
                        $quoteItem->setOriginalCustomPrice($orderItem->getPrice());
                        $quoteItem->getProduct()->setIsSuperMode(true);
                    }
                }
            }
        }
    }

}
