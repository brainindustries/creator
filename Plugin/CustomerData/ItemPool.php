<?php

namespace Shirtplatform\Creator\Plugin\CustomerData;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProduct;
use Shirtplatform\Core\Model\Config\Source\ProductType;

class ItemPool {    
    /**     
     * @var \Shirtplatform\Creator\CustomerData\DesignedItem
     */
    private $_designedItem;
    
    /**
     *     
     * @param \Shirtplatform\Creator\CustomerData\DesignedItem $designedItem
     */
    public function __construct(\Shirtplatform\Creator\CustomerData\DesignedItem $designedItem) {
        $this->_designedItem = $designedItem;
    }
    
    /**
     * Get item data for product. All designed images are returned for designed product.
     * 
     * @access public
     * @param \Magento\Checkout\CustomerData\ItemPoolInterface $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return array
     */
    public function aroundGetItemData($subject, $proceed, $item) {                
        if ($item->getShirtplatformId() and $item->getProductType() == ConfigurableProduct::TYPE_CODE) {
//            $product = $item->getProduct();
            
            if ($item->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
                return $this->_designedItem->getItemData($item);
            }            
        }
        
        return $proceed($item);
    }
}