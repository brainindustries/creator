<?php

namespace Shirtplatform\Creator\Plugin\Quote;

use Shirtplatform\Core\Model\Config\Source\ProductType;

class Item {
    /**
     * If the item is a base product and already has assigned shirtplatform_id return false, because
     * it may have different design
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Item $subject
     * @param boolean $result
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean
     */
    public function afterRepresentProduct($subject, $result, $product) {
        if ($result and $product->getShirtplatformProductType() == ProductType::BASE_PRODUCT and $subject->getShirtplatformId()) {
            return false;
        }
        
        return $result;
    }
}