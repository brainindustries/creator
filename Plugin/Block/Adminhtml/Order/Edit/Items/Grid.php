<?php

namespace Shirtplatform\Creator\Plugin\Block\Adminhtml\Order\Edit\Items;

use Shirtplatform\Core\Model\Config\Source\ProductType;

use Shirtplatform\Core\Helper\Image;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\App\RequestInterface;

class Grid {

    const ITEM_IMAGE_TEMPLATE = 'Shirtplatform_Creator::order/create/items/image.phtml';

    /**
     * @var Image
     */
    private $_imageHelper;

    /**
     * @var LayoutInterface
     */
    private $_layout;

    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * 
     * @param Image $imageHelper
     * @param LayoutInterface $layout
     * @param RequestInterface $request
     */
    public function __construct(
        Image $imageHelper,
        LayoutInterface $layout,
        RequestInterface $request
    ) {
        $this->_imageHelper = $imageHelper;
        $this->_layout = $layout;
        $this->_request = $request;
    }

    /**
     * Get quote item image HTML. For shirtplatform base product it renders
     * different template or call the original method if there are no designed
     * images
     * 
     * @access public
     * @param \BrainIndustries\OrderEditor\Block\Adminhtml\Order\Edit\Items\Grid $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @return string
     */
    public function aroundGetItemImageHtml($subject, $proceed, $quoteItem) 
    {
        if ($quoteItem->getShirtplatformId() and $quoteItem->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
            $designImages = $this->_imageHelper->getDesignImages($quoteItem, ['width' => 135, 'height' => 135], true);
            $mouseOverImages = $this->_imageHelper->getDesignImages($quoteItem, ['width' => 200, 'height' => 220]);

            if (!empty($designImages)) {
                $block = $this->_layout
                        ->createBlock(\Magento\Framework\View\Element\Template::class, 'quote_item_image_' . $quoteItem->getId())
                        ->setTemplate(self::ITEM_IMAGE_TEMPLATE)
                        ->setDesignImages($designImages)
                        ->setMouseOverImages($mouseOverImages)
                        ->setAlt($quoteItem->getName());

                return $block->toHtml();
            }
        }

        return $proceed($quoteItem);
    }

    /**
     * Get HTML for configure button. For shirtplatform base product it returns 
     * "Creator" button which opens creator in modal window on click.
     * 
     * @access public
     * @param \BrainIndustries\OrderEditor\Block\Adminhtml\Order\Edit\Items\Grid $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return string
     */
    public function aroundGetConfigureButtonHtml($subject,
                                                 $proceed,
                                                 $item) {
        if ($item->getShirtplatformProductType() == ProductType::BASE_PRODUCT) {
            $options = [
                'label' => __('Creator'),                
                'class' => 'btn-creator'
            ];
            
            //for ajax actions creator is already loaded => we can show the button
            $actionName = $this->_request->getFullActionName();
            if (in_array($actionName, ['sales_order_edit_index', 'sales_order_create_index'])) {
                $options['disabled'] = 'disabled';
            }

            if ($item->getShirtplatformId() and $item->getShirtplatformUuid()) {
                $options['onclick'] = sprintf('creator.editItem(%s, %s)', $item->getShirtplatformId(), "'" . $item->getShirtplatformUuid() . "'");
            }
            elseif (!$item->getShirtplatformId()) {
                $product = $item->getProduct();
                
                $childrenItems = $item->getChildren();                        
                if ($childrenItems) {
                    $firstChild = reset($childrenItems);
                    $childProduct = $firstChild->getProduct();
                    $options['onclick'] = sprintf('creator.configureItem(%s, %s, %s)', $item->getId(), $product->getShirtplatformId(), $childProduct->getShirtplatformColorId());
                }
                else {
                    $options['onclick'] = sprintf('creator.configureItem(%s, %s, null)', $item->getId(), $product->getShirtplatformId());
                }                
            }

            return $this->_layout->createBlock(\Magento\Backend\Block\Widget\Button::class)
                            ->setData($options)
                            ->toHtml();
        }

        return $proceed($item);
    }

}
