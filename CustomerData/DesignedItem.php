<?php

namespace Shirtplatform\Creator\CustomerData;

use Magento\Quote\Model\Quote\Item;
use shirtplatform\resource\PublicResource;
use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;

class DesignedItem extends \Magento\Checkout\CustomerData\DefaultItem {

    /**
     *
     * @var \Shirtplatform\Core\Helper\Image
     */
    private $_coreImageHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * View config model
     *
     * @var \Magento\Framework\View\ConfigInterface
     */
    private $_viewConfig;

    /**
     * 
     * @param \Shirtplatform\Core\Helper\Image $coreImageHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\View\ConfigInterface $viewConfig
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Msrp\Helper\Data $msrpHelper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Catalog\Helper\Product\ConfigurationPool $configurationPool
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param \Magento\Framework\Escaper $escaper
     * @param ItemResolverInterface $itemResolver
     */
    public function __construct(\Shirtplatform\Core\Helper\Image $coreImageHelper,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Framework\View\ConfigInterface $viewConfig,
                                \Magento\Catalog\Helper\Image $imageHelper,
                                \Magento\Msrp\Helper\Data $msrpHelper,
                                \Magento\Framework\UrlInterface $urlBuilder,
                                \Magento\Catalog\Helper\Product\ConfigurationPool $configurationPool,
                                \Magento\Checkout\Helper\Data $checkoutHelper,
                                \Magento\Framework\Escaper $escaper = null,
                                ItemResolverInterface $itemResolver = null) {
        parent::__construct($imageHelper, $msrpHelper, $urlBuilder, $configurationPool, $checkoutHelper, $escaper, $itemResolver);
        $this->_coreImageHelper = $coreImageHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_viewConfig = $viewConfig;
    }

    /**
     * Get quote item data for minicart. Add designed_product product type and 
     * designed images.
     * 
     * @access protected
     * @return array
     */
    protected function doGetItemData() {
        $itemData = parent::doGetItemData();
        $itemData['product_type'] = 'designed_product';
        $itemData['shirtplatform_design_group'] = $this->item->getShirtplatformDesignGroup();
        $itemData['designed_images'] = [];
        $itemData['mouseover_images'] = [];

        $quote = $this->checkoutHelper->getQuote();
        $excludeHumanView = $this->_scopeConfig->getValue(
                'shirtplatform/images/exclude_human_view_from_minicart', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $quote->getStoreId()
        );
        $designedImagesVars = $this->_viewConfig->getViewConfig()->getVarValue('Shirtplatform_Creator', 'designed_images');        
        $mouseoverImagesVars = $this->_viewConfig->getViewConfig()->getVarValue('Shirtplatform_Creator', 'mouseover_images');
        
        if (isset($designedImagesVars['width']) && isset($designedImagesVars['height'])) {
            $images = $this->_coreImageHelper->getDesignImages($this->item, ['width' => $designedImagesVars['width'], 'height' => $designedImagesVars['height']], $excludeHumanView);
        }
        else {
            $images = $this->_coreImageHelper->getDesignImages($this->item, ['width' => 70, 'height' => 70], $excludeHumanView);
        }

        if (isset($mouseoverImagesVars['width']) && isset($mouseoverImagesVars['height'])) {
            $mouseoverImages = $this->_coreImageHelper->getDesignImages($this->item, ['width' => $mouseoverImagesVars['width'], 'height' => $mouseoverImagesVars['height']], $excludeHumanView);
        }
        else {
            $mouseoverImages = $this->_coreImageHelper->getDesignImages($this->item, ['width' => 150, 'height' => 150], $excludeHumanView);
        }        

        //the result subarray should be indexed from 0 due to knockout template
        foreach ($images as $_image) {
            $itemData['designed_images'][] = $_image;
        }
        foreach ($mouseoverImages as $_image) {
            $itemData['mouseover_images'][] = $_image;
        }

        return $itemData;
    }

}
