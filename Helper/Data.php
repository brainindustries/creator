<?php

namespace Shirtplatform\Creator\Helper;

use Magento\Framework\App\State;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Shirtplatform\Core\Model\User\CookieUserFactory;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use shirtplatform\entity\order\Order;

class Data extends \Shirtplatform\Core\Helper\Data
{
    /**
     * @var State
     */
    private $_appState;

    /**
     * @param Context $context
     * @param State $appState
     * @param \Psr\Log\LoggerInterface $errorLogger
     * @param Filesystem $filesystem
     * @param \Psr\Log\LoggerInterface $logger
     * @param TransportInterface $mailTransport
     * @param CookieUserFactory $cookieUserFactory
     * @param EncryptorInterface $encryptor
     * @param DirectoryList $directoryList
     * @param File $ioFile
     * @param DateTime $dateTime
     */
    public function __construct(
        Context $context,
        State $appState,
        \Psr\Log\LoggerInterface $errorLogger,
        Filesystem $filesystem,
        \Psr\Log\LoggerInterface $logger,
        TransportInterface $mailTransport,
        CookieUserFactory $cookieUserFactory,
        EncryptorInterface $encryptor,
        DirectoryList $directoryList,
        File $ioFile,
        DateTime $dateTime
    ) {
        parent::__construct($context, $errorLogger, $filesystem, $logger, $mailTransport, $cookieUserFactory, $encryptor, $directoryList, $ioFile, $dateTime);
        $this->_appState = $appState;
    }

    /**
     * @param Order $platformOrder
     * @return array
     */
    public function getNonReorderableProducts(Order $platformOrder)
    {
        $result = [];
        foreach ($platformOrder->getOrderedProducts() as $_orderedProduct) {
            $_product = $_orderedProduct->design->product;
            foreach ($_product->getSku() as $_productSku) {
                if (!$_productSku->available || !$_product->active) {
                    $result[$_product->id][$_productSku->assignedSize->id][] = $_productSku->assignedColor->id;
                }
            }
        }
        return $result;
    }

    /**
     * @param \Magento\Sales\Model\Order\Item[] $orderItems
     * @param \shirtplatform\entity\order\Order $platformOrder
     * @return \Magento\Sales\Model\Order\Item[]
     */
    public function getProductsToReorder($orderItems, $platformOrder)
    {
        $result = [];
        $nonReorderableProducts = $this->getNonReorderableProducts($platformOrder);
        foreach ($orderItems as $item) {
            if ($item->getProductType() == 'simple') {
                $product = $item->getProduct();

                $shirtplatformId = preg_replace('/(^\D+|-\d+)/', '', $item->getSku());
                $shirtplatformColorId = $product->getShirtplatformColorId();
                $shirtplatformSizeId = $product->getShirtplatformSizeId();

                $canReorder = !in_array($shirtplatformColorId, $nonReorderableProducts[$shirtplatformId][$shirtplatformSizeId] ?? []);
                if (!$canReorder) {
                    continue;
                }
                
                if ($item->getParentItem() !== null) {
                    if ($this->_appState->getAreaCode() == \Magento\Framework\App\Area::AREA_FRONTEND && empty($item->getParentItem()->getPrice())) {
                        continue;                        
                    }
                    $result[] = $item->getParentItem();
                }

                $result[] = $item;
            }
        }
        return $result;
    }
}