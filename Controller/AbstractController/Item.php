<?php

namespace Shirtplatform\Creator\Controller\AbstractController;

use Magento\Checkout\Model\Cart as CartModel;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Shirtplatform\Creator\Helper\Cart as CartHelper;
use Shirtplatform\Creator\Helper\Data as DataHelper;

abstract class Item implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var CartModel
     */
    protected $_cart;

    /**
     * @var CartHelper
     */
    protected $_cartHelper;

    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * @var EventManager
     */
    protected $_eventManager;

    /**
     * @var MessageManager
     */
    protected $_messageManager;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var ResultFactory
     */
    protected $_resultFactory;    

    /**
     * @var SerializerInterface
     */
    protected $_serializer;

    /**
     * 
     * @param Context $context
     * @param CartModel $cart
     * @param CartHelper $cartHelper
     * @param DataHelper $dataHelper
     * @param SerializerInterface $serializer         
     */
    public function __construct(
        Context $context,
        CartModel $cart,
        CartHelper $cartHelper,
        DataHelper $dataHelper,
        SerializerInterface $serializer               
    ) {        
        $this->_cart = $cart;
        $this->_cartHelper = $cartHelper;
        $this->_dataHelper = $dataHelper;
        $this->_eventManager = $context->getEventManager();
        $this->_messageManager = $context->getMessageManager();        
        $this->_request = $context->getRequest();
        $this->_resultFactory = $context->getResultFactory();
        $this->_serializer = $serializer;
    }

    /**
     * Get POST data about the added item(s)
     * 
     * @access protected
     * @return array
     */
    protected function _getData()
    {
        if ($this->_request->getHeader('Content-Type') == 'application/json') {
            return $this->_serializer->unserialize($this->_request->getContent());
        }
        else {
            return $this->_request->getPost('data');
        }
    }
}