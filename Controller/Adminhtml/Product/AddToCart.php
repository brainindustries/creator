<?php

namespace Shirtplatform\Creator\Controller\Adminhtml\Product;

class AddToCart extends \Magento\Backend\App\Action {

    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magento_Sales::actions_edit';

    /**
     * @var \Shirtplatform\Creator\Helper\Cart
     */
    protected $_cartHelper;

    /**
     * @var \Shirtplatform\Creator\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * Quote session object
     *
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $_sessionQuote;

    /**
     * 
     * @param \Shirtplatform\Creator\Helper\Cart $cartHelper
     * @param \Shirtplatform\Creator\Helper\Data $dataHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(\Shirtplatform\Creator\Helper\Cart $cartHelper,
                                \Shirtplatform\Creator\Helper\Data $dataHelper,
                                \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
                                \Magento\Backend\Model\Session\Quote $sessionQuote,
                                \Magento\Backend\App\Action\Context $context) {
        parent::__construct($context);
        $this->_cartHelper = $cartHelper;
        $this->_dataHelper = $dataHelper;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_sessionQuote = $sessionQuote;
    }

    /**
     * Add shirtplatform variants to magento cart
     * 
     * @access public
     * @return \Magento\Framework\Controller\Result\Json
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute() {
        if (!$this->_request->isAjax()) {
            $this->_redirect('/');
            return;
        }

        $response = [];
        $resultJson = $this->_resultJsonFactory->create();
        $data = $this->_request->getPost('data');

        if (empty($data['originalQuoteItemId']) or empty($data['variants'])) {
            if ($data instanceof \Zend\Stdlib\ParametersInterface) {
                $params = $data->toString();
            }
            else {
                $params = $data;
            }

            $response['error'] = 1;
            $response['message'] = __('Cannot add product to cart');
            $logMessage = 'Cannot add product to cart. Post params:';
            $this->_dataHelper->logMessage($logMessage);
            $this->_dataHelper->logMessage($data);
            $this->_dataHelper->logError($logMessage);
            $this->_dataHelper->logError($data);
            $resultJson->setData($response);
            return $resultJson;
        }

        try {
            $this->_sessionQuote->getQuote()->removeItem($data['originalQuoteItemId']);
            $count = $this->_cartHelper->addVariants($this->_sessionQuote->getQuote(), $data['variants']);
            if ($count == 0) {
                throw new \Magento\Framework\Exception\LocalizedException(__('The product could not be added to your shopping cart'));
            }
        } catch (\Exception $ex) {
            $response['error'] = 1;
            $response['message'] = $ex->getMessage();
            $resultJson->setData($response);
            $this->messageManager->addError($ex->getMessage());
            return $resultJson;
        }

        if ($count == 1) {
            $message = __('You added 1 product to your shopping cart.');
        }
        elseif ($count >= 2 and $count <= 4) {
            $message = __('You added ' . $count . ' products to your shopping cart.');            
        }
        else {
            $message = __('You added %1 products to your shopping cart.', $count);            
        }

        $response['success'] = 1;
        $response['error'] = 0;
        $response['message'] = $message;
        $resultJson->setData($response);
        return $resultJson;
    }

}
