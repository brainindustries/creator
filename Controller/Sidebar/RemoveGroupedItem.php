<?php

namespace Shirtplatform\Creator\Controller\Sidebar;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\ResourceModel\Quote\Item as Resource;

class RemoveGroupedItem implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var CartRepositoryInterface
     */
    private $_cartRepository;
    
    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * @var Resource
     */
    private $_resource;

    /**
     * @var ResultFactory
     */
    private $_resultFactory;

    /**
     * @param CartRepositoryInterface $cartRepository
     * @param RequestInterface $request
     * @param Resource $resource
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        RequestInterface $request,
        Resource $resource,
        ResultFactory $resultFactory
    ) {
        $this->_cartRepository = $cartRepository;
        $this->_request = $request;
        $this->_resource = $resource;
        $this->_resultFactory = $resultFactory;
    }

    public function execute()
    {
        $result = $this->_resultFactory->create(ResultFactory::TYPE_JSON);

        try {

            $itemIds = $this->getItemIds();
            $quoteId = $this->_request->getParam('quote_id') ?? $this->getQuoteIdByItems($itemIds);

            $cart = $this->_cartRepository->get($quoteId);
            foreach ($itemIds as $_itemId) {
                $cart->removeItem($_itemId);
            }
            $this->_cartRepository->save($cart);

            $result->setData(['success' => true]);

        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'error_message' => $e->getMessage()
            ]);
        }

        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function getItemIds()
    {
        $itemIds = $this->_request->getParam('item_ids');
        if ($itemIds === null) {
            throw new \Exception('No Item IDs were sent.');
        }
        return json_decode($itemIds);
    }

    /**
     * @param array $itemIds
     * @return int
     */
    private function getQuoteIdByItems(array $itemIds) : int
    {
        $connection = $this->_resource->getConnection();
        
        $select = $connection->select()
            ->from($this->_resource->getMainTable(), 'quote_id')
            ->where('item_id IN (?)', $itemIds);
        
        return $connection->fetchOne($select);
    }
}
