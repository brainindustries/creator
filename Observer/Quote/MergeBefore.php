<?php

namespace Shirtplatform\Creator\Observer\Quote;

use Magento\Framework\Event\ObserverInterface;

class MergeBefore implements ObserverInterface {
    
    /**
     * Change shirtplatformDesignGroup for old quote items when merging quotes for guest
     * and logged in user to prevent grouping of non related items
     * 
     * @access public
     * @param Varien_Event_Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $sourceQuote = $observer->getSource();
        $destQuote = $observer->getQuote();        
        $maxDesignGroup = 0;                        
        
        //get max design group
        foreach ($destQuote->getAllItems() as $item) {            
            if ($item->getShirtplatformDesignGroup() and $item->getShirtplatformDesignGroup() > $maxDesignGroup) {
                $maxDesignGroup = $item->getShirtplatformDesignGroup();
            }
        }
        
        $nextDesignGroup = $maxDesignGroup + 1;
        $designGroups = [];
        
        //group source quote items by design group number
        foreach ($sourceQuote->getAllItems() as $item) {
            if ($item->getShirtplatformDesignGroup()) {
                $designGroups[$item->getShirtplatformDesignGroup()][] = $item->getId();
            }
        }                
        
        //assign next design group number to source quote items
        foreach ($designGroups as $oldItemIds) {
            foreach ($oldItemIds as $itemId) {
                $oldItem = $sourceQuote->getItemById($itemId);
                $oldItem->setShirtplatformDesignGroup($nextDesignGroup);                                
            }
            
            $nextDesignGroup++;
        }
    }
}