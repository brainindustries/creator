<?php

namespace Shirtplatform\Creator\Block\Product;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Checkout\Model\Cart;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Shirtplatform\Core\Helper\Data as CoreHelper;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\SalesRule\Model\RuleRepository;
use Magento\SalesRule\Model\ResourceModel\Rule as RuleResource;
use Magento\Variable\Model\Variable;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Creator extends \Magento\Framework\View\Element\Template
{

    const VAR_CREATOR_CONFIG = 'var_creator_config';

    /**
     * @var string
     */
    protected $_template = 'product/creator.phtml';

    /**
     * @var Cart
     */
    protected $_cart;

    /**
     * @var AdapterInterface
     */
    protected $_connection;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var CoreHelper
     */
    protected $_helper;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $_categoryRepository;

    /**
     * @var Variable
     */
    protected $_customVariable;

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var RuleRepository
     */
    protected $_ruleRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var Serializer
     */
    protected $_serializer;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * 
     * @param Cart $cart
     * @param Registry $coreRegistry
     * @param CoreHelper $helper
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ProductRepositoryInterface $productRepository
     * @param RuleRepository $ruleRepository
     * @param RuleResource $ruleResource
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SerializerInterface $serializer
     * @param Variable $customVariable
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Cart $cart,
        Registry $coreRegistry,
        CoreHelper $helper,
        CategoryRepositoryInterface $categoryRepository,
        ProductRepositoryInterface $productRepository,
        RuleRepository $ruleRepository,
        RuleResource $ruleResource,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SerializerInterface $serializer,
        Variable $customVariable,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_cart = $cart;
        $this->_coreRegistry = $coreRegistry;
        $this->_categoryRepository = $categoryRepository;
        $this->_productRepository = $productRepository;
        $this->_ruleRepository = $ruleRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_helper = $helper;
        $this->_connection = $ruleResource->getConnection();
        $this->_storeManager = $context->getStoreManager();
        $this->_serializer = $serializer;
        $this->_customVariable = $customVariable;
    }

    /**
     * Get current product
     * 
     * @access public
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('current_product');
        }
        return $this->_product;
    }

    /**
     * @return mixed
     */
    public function getOpenMotiveCategoryId()
    {
        return $this->getRequest()->getParam('motiveCat');
    }

    /**
     * @return int|null
     */
    public function getSharedProductId()
    {
        return $this->getRequest()->getParam('sharedProductId');
    }
    
    /**
     * Reads var_creator_config variable in admin, checks if it's a valid JSON and
     * returns this json.  Makes it possible to override default creator configuration
     * without the need of waiting until the platform is updated
     *
     * @access public
     * @return void
     */
    public function getCreatorConfig()
    {
        $this->_customVariable->setStoreId($this->_storeManager->getStore()->getId());
        $config = $this->_customVariable->loadByCode(self::VAR_CREATOR_CONFIG)->getPlainValue();
        try {
            $decodedConfig = $this->_serializer->unserialize($config);
        }
        catch (\InvalidArgumentException $ex) {
            return '{}';
        }

        return $config;
    }

    /**
     * Get URL of creator javascript file
     * 
     * @access public     
     * @return string
     */
    public function getCreatorJavascriptFile()
    {
        $store = $this->_storeManager->getStore();
        $creatorVersion = $this->_scopeConfig->getValue('shirtplatform/general/creator_version') ?: 'old';
        $url = $this->_scopeConfig->getValue('shirtplatform/general/url');

        if ($creatorVersion == 'old') {
            $url = preg_replace('/rest\/$/', '', $url);
            $url .= 'spring/magento/creator-app?shop=' . $this->_helper->getConnectionUniqUrl($store);
        } else {
            $url = preg_replace('/webservices\/rest\/$/', '', $url);
            $url .= 'creator/rest/magento/creator-widget?shop=' . $this->_helper->getConnectionUniqUrl($store);
        }

        return $url;
    }

    /**
     * Get URL of mini creator javascript file
     *
     * @access public
     * @param string $creatorId unique ID of mini creator
     * @return string
     */
    public function getMiniCreatorJavascriptFile($creatorId)
    {
        $store = $this->_storeManager->getStore();
        $creatorVersion = $this->_scopeConfig->getValue('shirtplatform/general/creator_version') ?: 'old';
        $url = $this->_scopeConfig->getValue('shirtplatform/general/url');

        if ($creatorVersion == 'old') {
            $url = preg_replace('/rest\/$/', '', $url);
            $url .= 'spring/magento/creator-app/mini?shop=' . $this->_helper->getConnectionUniqUrl($store) . '&creatorId=' . $creatorId;
        } else {
            $url = preg_replace('/webservices\/rest\/$/', '', $url);
            $url .= 'creator/rest/magento/creator-mini-widget?shop=' . $this->_helper->getConnectionUniqUrl($store) . '&creatorId=' . $creatorId;
        }

        return $url;
    }

    /**
     * Is this item edit page?
     * 
     * @access public
     * @return boolean
     */
    public function isItemEditPage()
    {
        return $this->getRequest()->getFullActionName() == 'checkout_cart_configure';
    }

    /**
     * Get current quote item
     * 
     * @access public
     * @return \Magento\Quote\Model\Quote\Item|null
     */
    public function getItem()
    {
        $item = null;
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            $item = $this->_cart->getQuote()->getItemById($id);
        }

        return $item;
    }

    /**
     * @return string
     */
    public function getChangeProductUrl()
    {
        return $this->_urlBuilder->getUrl('shirtplatform_creator/product/changeCallback');
    }

    /**
     * Get requested color id (shirtplatform color ID)
     * 
     * @access public
     * @return int|null
     */
    public function getRequestedColorId()
    {
        $color = null;
        $colorId = $this->getRequest()->getQueryValue('colorId');

        if ($colorId) {
            $product = $this->getProduct();
            $configurableOptions = $product->getTypeInstance()->getConfigurableOptions($product);

            foreach ($configurableOptions as $configurationId => $configuration) {
                foreach ($configuration as $configurationValue) {
                    if ($configurationValue['attribute_code'] == "color" and $configurationValue['value_index'] == $colorId) {
                        $variant = $this->_productRepository->get($configurationValue['sku']);
                        return $variant->getShirtplatformColorId();
                    }
                }
            }
        }

        return $color;
    }

    /**
     * @return string
     */
    public function getProductBaseUrl()
    {
        return rtrim($this->_urlBuilder->getUrl($this->getProduct()->getUrlKey() . '.html'), '/');
    }
    
    /**
     * Get discount rules
     *
     * @todo make it work with all operators. Now it's too specific
     * @access public
     * @param string $nameLike used in SQL condition to filter the rules
     * @return array
     */
    public function getDiscountRules($nameLike)
    {
        $isEnabled = $this->_scopeConfig->getValue('shirtplatform/general/creator_discounts');
        if (!$isEnabled) {
            return json_encode([]);
        }

        $rules = $this->_getRulesForWebsite($nameLike, $this->_storeManager->getWebsite()->getId());
        $result = [];
        foreach ($rules as $rule) {
            $conditions = $rule->getCondition()->getConditions();
            foreach ($conditions as $_condition) {
                if ($_condition->getOperator() == '>=') {
                    $_conditions = $_condition->getConditions() ?: [];
                    $exclusions = [];
                    foreach ($_conditions as $_cond) {
                        if ($_cond->getAttributeName() == 'sku' && $_cond->getOperator() == '!()') {
                            $exclusions = array_merge($exclusions, array_map('trim', explode(',', $_cond->getValue())));
                        } elseif ($_cond->getAttributeName() == 'category_ids' && $_cond->getOperator() == '!=') {
                            $exclusions = array_merge($exclusions, $this->_getProductSkusInCategory($_cond->getValue()));
                        }
                    }
                    $result[] = [
                        'qty' => $_condition->getValue(),
                        'amount' => (int)$rule->getDiscountAmount(),
                        'exclusions' => array_unique($exclusions),
                    ];
                }
            }
        }

        return json_encode($result);
    }

        
    /**
     * Get product SKUs in category
     *
     * @access private
     * @param int $categoryId
     * @return string[] SKUs
     */
    private function _getProductSkusInCategory($categoryId)
    {
        $skus = [];

        try {
            $category = $this->_categoryRepository->get($categoryId);
            $products = $category->getProductCollection();

            foreach ($products as $_product) {
                $skus[] = $_product->getSku();
            }
        } catch (NoSuchEntityException $ex) {
            $msg = 'Category ID ' . $categoryId . ' does not exist, but is set in common discounts cart price rules';
            $this->_helper->logError($msg);
        }

        return $skus;
    }

    /**
     * @access private
     * @param string $nameLike
     * @param int $websiteId
     * @return \Magento\SalesRule\Api\Data\RuleInterface[]
     */
    private function _getRulesForWebsite($nameLike, $websiteId)
    {
        $select = $this->_connection->select()
            ->from('salesrule_website')
            ->where('website_id = ?', $websiteId);

        $ruleIds = array_column($this->_connection->fetchAll($select), 'rule_id');

        $searchCriteria = $this->_searchCriteriaBuilder
            ->addFilter('is_active', 1)
            ->addFilter('name', $nameLike, 'like')
            ->addFilter('rule_id', $ruleIds, 'in')
            ->create();

        return $this->_ruleRepository->getList($searchCriteria)->getItems();
    }
}
