<?php

namespace Shirtplatform\Creator\Observer\Backend\Quote;

use Magento\Framework\Event\ObserverInterface;

class ProductAddAfter implements ObserverInterface {

    /**
     * @var \Shirtplatform\Creator\Helper\Cart
     */
    protected $_cartHelper;
    
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * 
     * @param \Shirtplatform\Creator\Helper\Cart $cartHelper     
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(\Shirtplatform\Creator\Helper\Cart $cartHelper,
                                \Magento\Framework\Registry $registry) {
        $this->_cartHelper = $cartHelper;
        $this->_coreRegistry = $registry;
    }

    /**
     * Add shirtplatform_id, shirtplatform_product_type, shirtplatform_orig_order_id, 
     * shirtplatform_uuid and custom price to quote item
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $variant = $this->_coreRegistry->registry('current_cart_variant_data');
        if ($variant) {
            $items = $observer->getItems();

            foreach ($items as $_item) {
                if (!$_item->getParentItem()) {
                    $this->_cartHelper->setVariantDataToQuoteItem($_item, $variant);                    
                    break;
                }
            }
        }
    }

}
